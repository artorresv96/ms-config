# Microservicio de configuraciones

_MS  que expone las url y otras configuraciones que pueden ser consumidas__por el ms cliente_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋

_Necesitas tener instalado **JAVA 8**   y  **Maven  3.6**_

```
java  =  https://openjdk.java.net/install/
maven =  https://maven.apache.org/ref/3.6.3/
```

### Instalación 🔧
_Ejecutar_

```
mvn clean  package
```

_Iniciar API_

```
java  -jar  target/*.jar
```

_Probar la API_

Iniciar el navegador de su equipo Chrome o Firefox  y con la url siguiente.

```
http://localhost:8090/ms-client/dev
```
